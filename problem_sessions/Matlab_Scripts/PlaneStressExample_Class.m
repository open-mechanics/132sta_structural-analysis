clc;
clear all;
close all;
%% Helpfull book
% Peter I. Kattan PhD (auth.)
% MATLAB Guide to Finite Elements_ An Interactive Approach-Springer-Verlag Berlin Heidelberg (2008)
%% Input parameter

% Symbols
syms a b X1 X2 E nu t1 t2 real;


% Numerical values
E_num = 210 *1e9; % Youngs modulus in N/m^2
nu_num = 0.3; % Poisson ratio
t_num = 0.025; % thickness in m
a_num = 0.125; % half length of elements in X1-direction in m
b_num = 0.125; % half length of elements in X2-direction in m

Coo = [0 0; 0 0.25; 0.25 0; 0.25 0.25; 0.5 0; 0.5 0.25];

D_pstress = [1 nu 0;...
             nu 1 0;...
             0 0 (1-nu)/2];
%D_pstress = E_num/(1-nu^2)*D_pstress;

%% Element stiffness matrix B^T*C*B
[k1,B] = elemstiffmat_planestress(E_num,nu_num,t_num,a_num,b_num);

%% Assembling the global stiffness matrix

k = zeros(8,8,2);
k(:,:,1) = k1;
k(:,:,2) = k1;

TDOF = [1 2 5 6 7 8 3 4;...
        5 6 9 10 11 12 7 8];
    
K_alt = zeros(12,12);
for iElem=1:2
    for iRow=1:8
        for iCol=1:8
            K_alt(TDOF(iElem,iRow),TDOF(iElem,iCol)) = K_alt(TDOF(iElem,iRow),TDOF(iElem,iCol)) + k(iRow,iCol,iElem);
        end
    end
end


%% solving the system of equations
D = zeros(12,1);
F = zeros(12,1);
F(9) = 9375; % in N
F(11) = 9375; % in N

% unknown displacements in m
D(5:12) = inv(K_alt(5:12,5:12))*F(5:12)

%% stresses in N/m^2

% element 1
D_elem1 = D(TDOF(1,:));
sigma_elem1 = elemstress_planestress(E_num,nu_num,a_num,b_num,B,D_elem1)

% element 2
D_elem2 = D(TDOF(2,:));
sigma_elem2 = elemstress_planestress(E_num,nu_num,a_num,b_num,B,D_elem2)


%% plotting the deformed system
scaling = 1e4;
D = scaling*D;
figure(1); hold on;
plot([Coo(1,1) Coo(2,1)],[Coo(1,2) Coo(2,2)],'k');
plot([Coo(3,1) Coo(4,1)],[Coo(3,2) Coo(4,2)],'k');
plot([Coo(5,1) Coo(6,1)],[Coo(5,2) Coo(6,2)],'k');
plot([Coo(1,1) Coo(3,1)],[Coo(1,2) Coo(3,2)],'k');
plot([Coo(3,1) Coo(5,1)],[Coo(3,2) Coo(5,2)],'k');
plot([Coo(2,1) Coo(4,1)],[Coo(2,2) Coo(4,2)],'k');
plot([Coo(4,1) Coo(6,1)],[Coo(4,2) Coo(6,2)],'k');

plot([Coo(1,1)+D(1) Coo(2,1)+D(3)],[Coo(1,2)+D(2) Coo(2,2)+D(4)],'r');
plot([Coo(3,1)+D(5) Coo(4,1)+D(7)],[Coo(3,2)+D(6) Coo(4,2)+D(8)],'r');
plot([Coo(5,1)+D(9) Coo(6,1)+D(11)],[Coo(5,2)+D(10) Coo(6,2)+D(12)],'r');
plot([Coo(1,1)+D(1) Coo(3,1)+D(5)],[Coo(1,2)+D(2) Coo(3,2)+D(6)],'r');
plot([Coo(3,1)+D(5) Coo(5,1)+D(9)],[Coo(3,2)+D(6) Coo(5,2)+D(10)],'r');
plot([Coo(2,1)+D(3) Coo(4,1)+D(7)],[Coo(2,2)+D(4) Coo(4,2)+D(8)],'r');
plot([Coo(4,1)+D(7) Coo(6,1)+D(11)],[Coo(4,2)+D(8) Coo(6,2)+D(12)],'r');

xlim([-0.1 0.6])
ylim([-0.1 0.35])


%% Function to assemble the elemnt stiffness matrix for plane stress
function [k,B] = elemstiffmat_planestress(E_num,nu_num,t_num,a_num,b_num)
% Element stiffness matrix B^T*C*B

% Symbols
syms a b X1 X2 E nu real;

% B-matrix
B(1,1) = -1*(b-X2);
B(1,3) = +1*(b-X2);
B(1,5) = +1*(b+X2);
B(1,7) = -1*(b+X2);

B(2,2) = -1*(a-X1);
B(2,4) = -1*(a+X1);
B(2,6) = +1*(a+X1);
B(2,8) = +1*(a-X1);

B(3,1) = -1*(a-X1);
B(3,2) = -1*(b-X2);
B(3,3) = -1*(a+X1);
B(3,4) = +1*(b-X2);
B(3,5) = +1*(a+X1);
B(3,6) = +1*(b+X2);
B(3,7) = +1*(a-X1);
B(3,8) = -1*(b+X2);
B = B/(4*a*b);

% matrix of material coefficients 
D_pstress = [1 nu 0;...
             nu 1 0;...
             0 0 (1-nu)/2];

% element stiffness matrix
k1 = E/(1-nu^2)*B'*D_pstress*B;

k1 = int(k1,X1,-a,a); % integration over X1 between the -a and +a
k1 = int(k1,X2,-b,b); % integration over X2 between the -b and +b
k1 = subs(k1,a,a_num); % substituting the symbal a with the value a_num
k1 = subs(k1,b,b_num); % substituting the symbal b with the value b_num
k1 = subs(k1,E,E_num); % substituting the symbal E with the value E_num
k1 = subs(k1,nu,nu_num); % substituting the symbal nu with the value nu_num
k1 = k1*t_num; % multiplying k1 with the thickness t_num
k = double(k1);
end

%% Function to calculate the stresses at the center of an element (sigma_x sigma_y sigma_xy)^T
function [sigma] = elemstress_planestress(E_num,nu_num,a_num,b_num,B,D)

% Symbols
syms a b X1 X2 E nu real;

% matrix of material coefficients 
D_pstress = [1 nu 0;...
             nu 1 0;...
             0 0 (1-nu)/2];

sigma = E/(1-nu^2)*D_pstress*B*D;
%sigma_elem1 = B*D_elem1;
sigma = subs(sigma,a,a_num); % substituting the symbol a with the value a_num
sigma = subs(sigma,b,b_num); % substituting the symbol b with the value b_num
sigma = subs(sigma,E,E_num); % substituting the symbol E with the value E_num
sigma = subs(sigma,nu,nu_num); % substituting the symbol nu with the value nu_num
sigma = subs(sigma,X1,0); % substituting the symbol X1 with 0 (stresses at X1 = 0)
sigma = subs(sigma,X2,0); % substituting the symbol X2 with 0 (stresses at X2 = 0)
sigma = double(sigma);

end
