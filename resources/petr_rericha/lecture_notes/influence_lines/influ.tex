\documentclass[12pt]{article}
\usepackage{graphics,my,comb}
\usepackage[cp1250]{inputenc}
\usepackage{makeidx}
\graphicspath{{figures/}}
%
% octave programs for examples are in files examframe.m  gridexamples.m
% that should be archived with 
% ~/bin/extr_tar.bsh displ-frames examframe.m  gridexamples.m
%
%\newlength{\figwidth}
%\newcommand{\insinFig}[3]
%   {\makebox[0pt][l]{\raisebox{-#1\figwidth}{\hspace{#2\figwidth} #3}}}
%\newcommand{\insFig}[2] 
%   {\makebox[#2][l]{\resizebox{#2}{!}
%   {\rotatebox{-90}{\includegraphics{#1}}}}}

\oddsidemargin 0mm

%\newtheorem{figsnum}{\small \rm Sketch}
%\newtheorem{statement}{\rm Rule}[section]
%\newtheorem{princ}{\rm Principle}
%\newtheorem{tabl}{\rm Table}
\renewcommand\contentsname{Contents}
\renewcommand{\sketch}{figure}
%\newcommand{\arule}{Rule}
%\newcommand{\ccir}[1]{\makebox[0pt][l]{$\bigcirc$}~#1~~}
%\newcommand{\correction}[1]{{\ttfamily #1}}
%%\newcommand{\correction}[1]{ #1}
\usepackage{times}
\makeindex
\listfiles
%\flushbottom
%\pagestyle{headings}

% the page size required by CVUT Edition center:
\topmargin -24mm
\textheight 250mm
%\textwidth 160mm
\oddsidemargin 0mm
\footskip 10mm
\pagestyle{plain}
\textfloatsep 0pt
% for .pdf edition:
\topmargin -24mm
\textheight 230mm
\textwidth 180mm
\oddsidemargin -10mm
\footskip 0mm
\pagestyle{plain}
\textfloatsep 0pt
\listfiles

\begin{document}
\title{Structural analysis - influence lines 
in planar statically determinate frames}
\author{Petr \v Re\v richa }
%\month{May}
%\year{2002}
%\date{}
\maketitle
\tableofcontents
\section{Extreme live load effects}
The load carrying capacity of a structure mostly is assessed on the
level of cross-section variables -- internal forces and deflections.
It is required for every cross-section that the 
\index{forces!internal!extreme}%
extreme internal
forces which any possible load configuration can bring about are less
than the corresponding cross-section strengths. Analogous
conditions  apply to deflections and reactions.
In assessing the strength of a structure, it is seldom
sufficient to compute reactions and internal forces for a single
load case. The most adverse distribution 
of the 
\index{load!live}%
live load has to be found
with respect to a given quantity which may be a reaction component,
an internal force at a given cross-section, deflection of a point
and further quantities. A load distribution which entails the extreme
bending momemt at a section is different from the distribution that
invokes the extreme at some other cross-section. All these extreme
effects and corresponding load distributions should be  determined
for every important quantity. This is a formidable task even in
simple problems, keep in mind that there are infinitely 
many cross-sections.
Several critical cross-sections and quantities are selected in practice
on the basis of experience and engineering judgment and the
extreme load effects are then evaluated just for them. The set of
load cases  to be considered also can be very extensive. It may
even be a continuous uncountable set in case of moving loads -- like
traffic loads are.  The search for the most adverse distributions
of the load
can be facilitated  by influence lines. Most applications of
influence lines occur in bridges and other traffic structures.
\index{bridge}%

\section{Purpose and application of influence lines}
An influence line always is associated with a definite quantity.
In order to distinguish it from the values of the same quantity
in the standard diagrams,
the lower case Greek $\eta$ is prepended to the quantity symbol.
The influence line $\eta Q(x)$ of a quantity $Q$ consists of
ordinates $\eta Q(x)$ defined at positions $x$ along the beam
axes. Variable $x$
is used to parametrize positions where 
\index{load!live}%
a live load can act.
An example of a bridge structure is shown in~\sketch~\ref{influ1}.
\index{bridge}%
The 
\index{load!live}%
live load can act on the roadway slab where the influence lines will
be defined. \\
\insFigUp{influ1.pdf}{\textwidth}\\
\begin{figsnum}
Definition of influence line. Two ordinates of each of 
two influence lines are indicated by circles.
  \label{influ1}
\end{figsnum}
Two instances of quantities, bending moment $M_a$ at cross-section $a$
and reaction $B$ are indicated in the \sketch. These two quantities
represent many others that could be of interest in this example.

The ordinate~$\eta Q(x)$
is the magnitude of~$Q$ when a 
\index{force!unit}%
unit force of a given 
direction acts at the position~$x$ as an exclusive load.
The direction is the same for all positions~$x$.
Potentially any direction of the unit force is possible but a great
majority of 
\index{load!live}%
live loads are vertical downward loads and 
this is the default
direction of the unit force. Two positions of the unit force are
indicated in the figure together with ordinates of two quantities whose
influence functions are being drawn. The dashed curves represent
parts of the influence lines of the two quantities when the unit force
travels from its initial position in the left drawing to its
position in the right one. When the unit force travels along the
roadway beam functions $\eta M_a(x)$ and $\eta B(x)$ are obtained --
influence lines of the two quatities. 

With an influence line known  the effect of arbitrary vertical load
upon the roadway beam can be evaluated by superposition. Bending
\index{roadway}%
moment $M_a$ in the left diagram in~\sketch~\ref{influ2} is
\[ M_a= F~\eta M_a(x) \]
The effect of other concentrated forces is evaluated similarly. 
The effect of
continuous loading is expressed as an integral effect of the
differential substitute force $f(x)dx$ indicated in the right
diagram of~\sketch~\ref{influ2}:
\begin{equation}
 M_a= \int f(x)~\eta M_a(x)~\dd x                 \label{eq_infl1}
\end{equation}
\setfigwidth{0.8\textwidth}
\figdata{influ2.pdf}
\bemifi
\insFigU%
\begin{figsnum}
Evaluation of a bending moment due to a concentrated force and
continuous loading. The influence line is tentative, curved influence
lines occur in statically indeterminate structures.
                                                  \label{influ2}
\end{figsnum}
\enmifi

Influence lines serve two basic purposes and usually are used
in corresponding two steps,
\begin{itemize}
\item
to determine 
\index{load!live}%
live loads positions resulting in extreme effects 
(mind that different positions may result for different quantities)
\item
to evaluate the extreme effects.
\end{itemize}
An illustrative problem is presented in \sketch~\ref{syllinfl1}.
The task is to determine maximum $M_c$ for any location of the
force and uniform load. The first question naturally is where 
to place the uniform load and the point force.\\
\setfigwidth{0.3\textwidth}
\figdata{syllinfl1.pdf}
\bemifi
\insFigU%
\begin{figsnum} Purpose of influence lines -- sample problem
                                                    \label{syllinfl1}
\end{figsnum}
\enmifi%
\becol
The uniform load $f$ potentially may be placed anywhere 
on any length (or lengths). This sort of 
\index{load!live}%
live load is specified in all
building codes and represents an arbitrarily distributed live load
of a given intensity.  Needless to say, the load locations indicated in 
\sketch~\ref{syllinfl1} are just tentative. Correct locations could
certainly be determined by 'trial and error' method in this simple
example. However, with regard to the practical importance of the task, 
a systematic approach is desirable.
\encol\\
The task can be solved by means of the 
influence line of $M_c$, denoted by~$\eta M_c$.
It is assumed that the influence line used in~\sketch~\ref{syllinfl1a}
had been constructed as shown in \sketch~\ref{influ4}.\\
\setfigwidth{0.3\textwidth}
\bemifi
\figdata{syllinfl1a.pdf}
\insFigU%
\begin{figsnum}
Influence line and its application
                                                    \label{syllinfl1a}
\end{figsnum}
\enmifi%
\becol
It is apparent that the
most effective position of $P$ is at $C$ and the uniform
load should stretch from $A$ to $B$. The other (negative)
extreme value $M_c$ is obtained when $P$ is positioned at $D$
and $f$ from $B$ to $D$. Both load positions are shown in 
\sketch~\ref{syllinfl1a}. The example is trivial enough
and could have been solved without the aid of the influence line.
A  more difficult task follows when
the uniform load has a fixed length and intensity and its position
remains variable (this is  the case of an exclusive vehicle train
which appears in all bridge building codes).
\index{bridge}%
Then the task can hardly be solved without the influence line.
The effect of such load is obtained by the integral~\refpar{eq_infl1}
which equals in this case the shaded area under the influence line
in the bottom diagram of~\sketch~\ref{syllinfl1a}. 
Even with the aid of the
influence line it is not trivial to detect the most effective position
of the uniform load.
\encol\\
Once the most effective distribution of the loads has been determined
the extrem values of the respective quantity  can be calculated 
\begin {itemize}
\item either solving the structure for the given distribution
 of loads (now known)
\item or by superposition and/or integration using the influence line
\end {itemize}

\section{Construction of influence lines}
The basic method of influence lines construction follows from their
definition -- for any given quantity line a  
\index{force!unit}%
unit force of the given 
(usually vertical) direction is
moved along the structure and values of the quantity for each position
of the force constitute the influence line.
This might be difficult when taken literally -- infinitely many
positions of the unit force are to be considered.
The influence line is thus developed in form of a function of the
unit force position~$x$.\\
\setfigwidth{0.4\textwidth}
\bemifi
\figdata{influ3.pdf}
\insFigU%
\begin{figsnum}
Influence lines by definition.
                                                      \label{influ3}
\end{figsnum}
\enmifi%
\becol
The reaction~$A_y$ is 
\[ A_y= { l - x \over l } \]
for any position of the unit force. Similarly
\[ B= {x \over l} \]
These functions define the two respective influence lines shown
in the figure.
\encol\\
The influence line of a bending moment at a given cross-section
is more complicated. In the example the influence line
$\eta M_C$ consists of two functions valid in different parts of the
beam. \\
\setfigwidth{0.4\textwidth}
\bemifi
\figdata{influ4.pdf}
\insFigU%
\begin{figsnum}
Influence line for the bending moment at a general cross-section
by definition.
   \label{influ4}
\end{figsnum}
\enmifi
\becol
When the force moves between~$A$ and $C$ 
\[ M_C= B~(l-\xi)= (l-\xi){x \over l}~~~~~0< x <\xi \]
whereas
\[ M_C= A~\xi= \xi~{1-x \over l}~~~~~\xi < x  \]
is valid when the force is beynd~$C$.
\encol\\
Influence lines might become substantially more complicated in
complex frames. They all share a very useful common feature, however. 
As the recent example demonstrates, they consist of straight
lines exclusively. This the consequence of the fact that they
are developed from equilibrium equations which are always
linear. No other than linear functions can emerge from such
equations. This feature is confirmed below through the application
of the principle of virtual displacements (PVd). 
The argument suggests that the feature may not be
valid in other than  statically determinate structures when other
than equilibrium equations are necessary to solve for reactions
and internal forces. Indeed, a important statement is valid:

\begin{statement}
Influence lines consist of straight lines in statically determinate
structures.
\end{statement}
The statement makes the construction of the influence lines
substantially easier since the ordinates need just be computed at the
ends of intervals with linear variation.

\section{Influence lines by Muller-Breslau principle}
Principle of virtual displacements (PVd) is very suitable
for the evaluation of various load cases in a single
statically determinate structure. Moving unit force in fact
produces a continuous set of loading cases and 
the associated evaluations
of the quantity in question can conveniently be carried out by the PVd.\\
\setfigwidth{0.4\textwidth}
\bemifi
\figdata{influ5.pdf}
\insFigU%
\begin{figsnum}
Virtual displacement to determine $C_x$
   \label{influ5}
\end{figsnum}
\enmifi%
\becol
The 
\index{force!unit}%
unit force may act anywhere upon the horizontal projection of the
structure in the next demonstration example in \sketch~\ref{influ5}.
The task is to construct
the influence line $\eta C_x$ of the horizontal component $C_x$ of the 
internal reaction $C$. 
The virtual displacement is developed in the standard way with
rotation centers $o_I$ and $o_{II}$. The relationship of the
virtual rotations is determined from the condition that the vertical
displacements of both rigid parts are the same at $C$ and equal
\[ \delta u_y^I=\delta u_y^{II}=\delta \phi_I~ a=-\delta \phi_{II}~b \]
\encol\\
The virtual work expressions become for the variable position $x$
\index{virtual work}%
of the unit force between hinges $A$ and $C$
 \[ \delta w = - \delta u_y~1 -C_x~h~\delta \phi_I =0 ,
~~~~~~ \delta u_y = x~\delta \phi_I, 
~~~~~~ \eta C_x(x)= -{x \over h}~~~\mbox{for}~~~0<x<a \]
and for the position between $C$ and $B$
\[ \delta u_y = - x'~\delta \phi_{II},
~~~~~~ \eta C_x(x')= -{x'~a \over b~h} ~~~\mbox{for}~~~ b>x'>0 \]
Variable $x$ does not denote the location of the cross-section!
Instead it denotes the location of the vertical unit loading force.
It is important to note that $\delta u_y$ (positive upwards) 
has a simple
graphical meaning -- it is the vertical component of the 
\index{displacement!virtual}%
virtual
displacement vector~$\delta \vec{u}$. Several vectors
$\delta \vec{u}$ are shown in~\sketch~\ref{influ5} for several
points on the beam $AC$. The shape of the influence line
diagram $\eta C_x$ is determined by the variation of 
this vertical component -- it is proportional to the distance $x$. 
In many
cases the shape is relatively easy to guess
and then it remains to compute the correct factor for full definiteness
of the influence line.

The previous argument also confirms that the influence lines consist
of straight lines since any component
of the virtual displacement vector always is proportional to the
distance of the loading point from the respective rotation center.

Influence lines for other 
\index{force!unit!directions of}%
directions of the unit force can
be derived from the known virtual displacement $\delta \vec{u}$
by projection on the respective direction. This is rather
exceptional in practice, however. Nevertheless, the above 
considerations can be contained in a verbal rule which a slight
extension of the M\"{u}ller-Breslau principle:
\begin{statement}
Ordinates of the influence line of a given quantity are the components
of the displacement vector of the deflected structure when the 
constraint conjugate to the quantity is released and unit
\index{constraint!conjugate}%
\index{displacement!conjugate}%
conjugate displacement is imposed. Components in a given direction
apply to the 
\index{load!live}%
live load in the same direction
\end{statement}

\section{Recommended practice}
A combination of the basic methods is recommended in practice.
The shape of the influence line is detected by the PVd. 
As a matter of fact,
this step  means looking up the rotation centers of the individual
rigid parts. In the course of this step, the straight parts of the
influence line are determined and their continuity, breaks and possible
discontinuities resolved. 
\index{discontinuity}%
The relationships among rotation angles need not
be detected as a rule. It then remains  to
determine a single multiplication factor of the line. 
This is best done when an ordinate of the influence line is
determined 'by definition'
i.e. locating the unit force at some point and solving for the
quantity value in this specific load case. 
The ordinate of the influence line is thus obtained
at the current force position which determines the factor. 
The solution  for specific locations of the unit force should be
carried out more than once to check up. \\
\setfigwidth{0.4\textwidth}
\bemifi
\figdata{influ6.pdf}
\insFigU%
\begin{figsnum}
Influence lines for a continuous beam.
                                                   \label{influ6}
\end{figsnum}
\enmifi
\becol
The continuous beam in \sketch~\ref{influ6} is an example 
of this influence lines construction. All rotation centers lie on the
axis of the beam since horizontal displacements are restrained
in the whole structure. But for exceptions the centers coincide with the
support points.
The exceptions are obvious -- when the influence
line of a reaction is developed the support point of that reaction
must move. This invokes the chain of rotations of adjacent members
as illustrated in \sketch~\ref{influ6}.
The magnitude of these rotations is
best determined when the unit force is located upon the respective
support (position $a$ upon $A$ in case of $\eta A$). This determines
the ordinate 1 of the influence line. Analogous construction applies
to reactions $B$ and $C$. The unit ordinates in these lines 
are obtained when unit force stands in positions $b$ and $c$
respectively. Keep in mind that for each influence line just one
of the indicated positions of the unit forces is used, in other words,
there is always just one unit force acting in each load case to
be considered. The top
diagram in the figure with four indicated locations is not a
load case! It shows the positions of the unit force in four
different load cases.
\encol\\
Influence lines of other quantities than vertical reactions
have the rotations centers above supports. 
The influence line of shear force $V_{BA}$ 
requires relative vertical displacement at cross-section $BA$.
The shape of the line is determined by this fact. Remember
that the slopes
in intervals $AB$ and $BC$ must be the same since the relative
rotation of these two parts is restrained at section $BA$.
The ordinate of the line is determined for unit force located
infinitely close left from cross-section $BA$ (the location cannot
be graphically distinguished from position $b$). 
Relative rotation
of parts $AE$ and $EB$ must be imposed to invoke the influence
line of bending moment $M_E$. The rotation centers above supports
entail the line shape. The ordinate at $E$ is best obtained when
the location $e$ of the unit force is used. Solution of the 
structure for this load case yields $A=1/2$, $B=1/2$ and other
reactions vanish. The ordinate $M_E=l/4$ is obtained.


%Rule: Impose a virtual displacement with unit displacement in the place
%and direction of the variable whose influence line is to be
%derived. The deflection line (consisting always of straight lines
%in case of statically determinate structures) is the desired influence
%line.\\
%Example: Influence line of the bending moment at section $C$ of the
%simple beam.\\
%Unit relative rotation is imposed between the two parts of the
%beam that are cut out by section $C$. Vertical components of the
%displacements are ordinates of the desired influence line.
%
%\epsfbox {syllinfl3.ps}
%\\
%Proof by principle of virtual displacements, for the above example
%it can be outlined as follows.  Virtual work done by all acting forces:\\
%$$\delta w=~M_C~\delta \varphi~+~1~\delta u=~0$$\\
%If $\delta \varphi =~1$ then $M_C=~-\delta u$.
%
%Note: 
%\begin{itemize}
%\item Influence lines could easily be obtained for any direction
%of the unit force by this method.
%\item From the kinematics of virtual displacements it is apparent
%that inluence lines consist of straight lines (when the members are
%rigid).
%\end{itemize}
%The last knowledge is used in the fourth (and recommendable)
%method of construction.\\
%Example:\\


\section{Examples of influence lines construction}
Various combinations of the two basic methods are used in practice.
It is best illustrated in examples. The examples are limited to
vertical
\index{load!live}%
live load moving along the horizontal girders of the respective frame
as indicated by the unit force positions in the frames schemes.
The reader is recommended to try to extend the influence lines to other
parts of the frame on his (her) own. Extension to other than vertical
live load also is recommendable. 
Commented examples suggest some ideas how the influence lines can
efficiently be constructed. Section~\ref{app_inlu}
offers further exercise.

\subsection{Example 1}
\setfigwidth{0.4\textwidth}
\bemifi
\figdata{infl_example.pdf}
\insiFig{0.0}{0.75}{$\eta A_x$}%
\insiFig{0.0}{0.5}{$\eta C_y$}%
\insiFig{0.0}{0.2}{$\eta M_d$}%
\insFigU%\\
\begin{figsnum}
Influence lines for a three-hinge arch.
   \label{infl_example}
\end{figsnum}
\enmifi%
\index{arch!three-hinge}%
\begin{colfill}{0mm}
The influence lines are constructed for a vertical force moving along
the horizontal beam.
For the line $\eta A_x$ the ifluence line must have
two straight parts (there are two rigid members)
which connect at $C$. It is then sufficient to position
the unit force at the three locations shown in the drawing
and calculate $A_x$ for these three load cases. Remember that
the two side positions of the unit force allow for trivial solutions
-- the force ray goes directly through the external hinge so that
all reactions vanish except $A_y$ or $B_y$ respectively.
Two trivial null ordinates of the influence line are thus detected.
The central position represents a nontrivial load case whose solution
yields $A_x=1/2$.


Line $\eta C_Y$ consists of two straight parts again
but they disconnect at $C$ in vertical direction. Four
positions shown are then necessary to construct the
line. The two central positions differ by infinitesimal shift of
the unit force. The asociated load case is the central position
solved already above. Exclusively $C_y$ changes with the shift.
Line $\eta M_d$ has three straight parts since member II
must be broken at $d$ to allow for relative rotation at this
point. The positions of unit force to determine necessary
values of $M_d$ are shown.
\encol\\
Note that just two nontrivial load cases need actually be 
solved (central position and the rightmost position).
The positions of the unit force shown in the example
are not the unique possible ones. It is also recommeded
to position the force in more than the necessary number of
locations for checking. 
%Sometimes the break in the influence line
%does not actually occur at a internal hinge as the next example

\subsection{Example 2}
\setfigwidth{0.4\textwidth}
\bemifi
\figdata{influ7.pdf}
\insFigU%\\
\begin{figsnum}
A frame structure and several influence lines.
   \label{influ7}
\end{figsnum}
\enmifi
\begin{colfill}{0mm}
Influence line for the reaction $D$ in the link consists of two
straight lines since the unit force travels along two rigid
parts. These two straight lines remain continuous at $C$.
Trivial null ordinates are obtained at points $A$ and $B$
where the unit force goes directly into the respective
support and no other reactions or internal forces occur in the 
structure. The ordinate at $C$ must be computed, preferably
by solving the structure for this position of the unit force:
\begin{center}
\setfigwidth{0.5\textwidth}
\figdata{influ8.pdf}
\insFigU%
\end{center}
\[ II~C~\odot~~0.5\cdot 10 +D\cdot 5 =0~~~~ \underline{D= -1}\]
The influence line for $M_{ED}$ also consists of two straight lines.
It can be derived from $\eta D$ easily when it is realized
that $M_{ED}= D\cdot 5$ when there is no loading between points
$D$ and $E$.
\encol\\
Line $\eta M_{EB}$ consists of three straight lines since the release
of the rotational constraint at cross-section $EB$ renders three
\index{constraint}%
rigid parts along the unit force track. Besides trivial ordinates
at $A$ and $B$, two nontrivial ordinates are to be computed,
for instance at $C$ and $E$. This makes two load cases to be solved.
One can be saved, however, when the respective virtual displacement
is imagined:\\
\setfigwidth{0.3\textwidth}
\begin{minipage}{\figwidth}
\figdata{influ9.pdf}
\insFigU%
\begin{figsnum}
Virtual displacement to determine $\eta M_{EB}$.
   \label{influ9}
\end{figsnum}
\enmifi
\begin{colfill}{0mm}
Parts $I$ and $II$ remain connected and rotate as a single rigid
part which implies that the $\eta M_{EB}$ is straight from $A$ to $E$.
The ordinate  of $\eta M_{EB}$ at $C$ follows from the load case shown
above:
\[ M_{EB}= B\cdot 5= 2.5 \]
\encol\\
Line $\eta M_{EC}$ consists of three straight lines and two nontrivial
load cases are to be considered again. This is left to the reader
for exercise. Another 'trick' can be demonstrated on this influence
line.
The moment equilibrium of the joint $E$ reads \\
\setfigwidth{0.2\textwidth}
\figdata{influ10.pdf}
\bemifi
\insFigU%
\enmifi
\begin{colfill}{0mm}
\[ M_{EB}+M_{ED}-M_{EC}=0 \]
for any location of the unit force. Line $\eta M_{EC}$ can thus be
obtained as the sum of the lines $M_{EB}$ and $M_{ED}$.
\encol\\
This concept can be extended in the sense that a new influence line
can often be obtained as a linear combination of several already
known lines. The factors of the linear combination follow from
suitable equilibrium conditions. In the above instance the factors
are 1 for both $M_{EB}$ and $M_{ED}$.



\section{Influence lines of member forces in trusses}
There is no principal difference in the construction of the influence
lines for frames and trusses and the same methods are used.
The nature of the task suggests that the method of sections
is usually preferred when it comes to determine a member force
in the basic method ('by definition') or in the combined
methods. Influence lines are frequently required in trusses with
at least one chord straight and horizontal. Most railway bridges
\index{chord}%
\index{bridge}%
were built in the past with planar trusses as the main load bearing
systems. When deciding on the straight parts of the influence line
in the combined methods one should recall that there are usually just
two or three rigid substructures after the constraint has been
\index{substructure}%
released in the PVd method.

\subsection{Example 1}
A rather typical example is considered here for illustration.
Influence lines of axial forces in truss members {\it b, c, d} are
to be determined for the force travelling along the upper chord.
The rigid substructures subject to virtual rotations are 
shaded in the inserted small schemes of the truss, separately
for each influence line and the associated 
\index{displacement!virtual}%
virtual displacement.
The positions of the unit force are indicated above each influence
line that have been used to compute the respective ordinates
and the computation course is outlined in the attached notes.\\
\setfigwidth{80mm}
\figdata{infl_example_truss.pdf}
\bemifi
\insiFig{0.1}{0.6}{$\eta N_c$}%
\insiFig{0.10}{0.48}{$-\sqrt{2} /4=-0.354$}%
\insiFig{0.30}{0.63}{$\sqrt{2} /2=0.707$}%
\insiFig{0.1}{0.35}{$\eta N_b$}%
\insiFig{0.27}{0.35}{$3/4$}%
\insiFig{0.1}{0.1}{$\eta N_d$}%
\insiFig{0.71}{0.12}{$3/4$}%
\insFigU%
\begin{figsnum}
Truss and influence lines for live load upon the lower chord
\index{chord}%
   \label{infl_example_truss}
\end{figsnum}
\enmifi%
\becol
Unit forces are placed at the two positions shown and
the associated axial forces $N_c$ are determined by the
section method. The appropriate cut is outlined in the
structure diagram. It is apparent that the force $N_c$
is zero for both limit positions of the unit force
above supports (these two positions are not shown.

\vspace{5mm}
Besides the two positions above supports, just one
position of the unit force is necessary since both
rigid parts may not move relatively at $C$. The
axial force $N_c$ is best determined  by the
section method using the same section as before.

\vspace{5mm}
The axial force for the indicated position of the unit
force can be determined by joint equilibrium or by the
section method again. For the latter case, the section
is shown in the structure diagram.
\encol\\

The combination of the PVd and the standard equilibrium equations
\index{PVd|)}%
is flexible and offers space for invention. In the next example
the live load acts along the lower chord of the truss.
\index{chord}%

\subsection{Example 2}
\setfigwidth{0.5\textwidth}
\begin{minipage}{\figwidth}
\figdata{influ11.pdf}
\insFigU%\\
\begin{figsnum}
Truss and influence lines for live load upon the lower chord
   \label{influ11}
\end{figsnum}
\enmifi
\becol
Two rigid substructures result when constraint $N_{2,3}$ is 
released. They are shown in~\sketch~\ref{influ13} 
in their displaced positions after
the virtual displacement has been imposed. Arrows show the
displacement vectors of two points of the lower chord. Vertical
components of the vectors are the ordinates of the influence
line $\eta N_{2,3}$. The triangle shape of the line can be detected.
 \\
\setfigwidth{1.0\textwidth}
\figdata{influ13.pdf}
\insFigU%\\
\begin{figsnum}
Virtual displacement to  determine the shape of $N_{2,3}$ \label{influ13}
\end{figsnum}
\encol\\
It remains to determine an ordinate. A specific load case is considered
for this purpose:\\
\setfigwidth{0.5\textwidth}
\begin{minipage}{\figwidth}
\figdata{influ12.pdf}
\insFigU%\\
\begin{figsnum}
The load case to  determine  an ordinate of $N_{2,4}$ \label{influ12}
\end{figsnum}
\end{minipage}
\becol
The equilibrium equation of the section method is applied to determine
$N_{2.3}$ after external reactions had been computed:
\[ II~6~\odot:~~0.25\cdot 12 -N_{2,3}~4 =0 ~~~~~\underline{N_{2,3}=0.75}\]
\encol\\

The rigid substructures that arise when constraint $N_{2,4}$ is
\index{constraint}%
released are shown with their respective virtual displacements.\\
\setfigwidth{0.5\textwidth}
\begin{minipage}{\figwidth}
\figdata{influ14.pdf}
\insFigU%\\
\begin{figsnum}
Virtual displacements to determine $N_{2,4}$ \label{influ14}
\end{figsnum}
\end{minipage}
\becol
Standard rules for the rotation centers allow for the determination
of $o_{I,III}$ when simple truss $II$ is perceived as a link between
\index{truss!simple}%
rigid parts $I$ and $III$. The centers $o_{I}$ and $o_{I,III}$
coincide, however, and give thus no clue to $o_{III}$. On the other
hand, their coincidence implies that $\delta \phi_{III}=0$ whatever
the location of $o_{III}$ is. Part $III$ does not move at all and
the virtual displacement consists of the rotations of parts $I$ and
$II$ as indicated in \sketch~\ref{influ14}. 
\encol\\
The shape of $\eta N_{2,4}$ follows from the virtual displacement 
in the above figure.
Member force $N_{2,4}$ needs now be determined for some 
specific position
of the unit force. It is advantageous to recall the load case already
used -- unit force at joint 2 in \sketch~\ref{influ12}. The section
cutting through members 5,6, 5,4, 2,4, 2,3 can be adopted to compute
$N_{2,4}$ (recall that $N_{2,3}$ is already known so that just three
unknown member forces remain in the section. Independent solution
for $N_{2,4}$ follows directly from the virtual displacement in
\sketch~\ref{influ4}:
\[ \delta w = -1\cdot 4~\delta \phi_{I} +N_{2,4}\cdot 2\sqrt{2}
  \delta \phi_{I} - N_{2,4}\cdot 2 \sqrt{2} \delta \phi_{I} =0 
  ~~~~~~\underline{N_{2,4}}= 0.707 \]
The influence line in~\sketch~\ref{influ11} is thus obtained.
The same sequence of steps could be used to determine $\eta N_{4,5}$.\\
\setfigwidth{0.5\textwidth}
\bemifi
\figdata{influ15.pdf}
\insFigU%\\
\begin{figsnum}
Virtual displacements to determine $N_{4,5}$ \label{influ15}
\end{figsnum}
\enmifi
\becol 
The corresponding virtual displacement pattern is
shown in~\sketch~\ref{influ15}. The shape of the line is apparent and
the ordinate at joint 2 is readily obtained from the equilibrium
of joint 4 in the direction 2-4.
\[ N_{4,5} \sqrt{2}/2 + N_{2,4}=0~~~~~~N_{4,5}= N_{2,4} \sqrt{2} \]
\encol\\
It is worth mentioning that the influence line $\eta N{4,5}$
could also be obtained
as a linear combination of the known ones (a simple multiple 
of a single one in this case) with the aid of the last equation.

\section{Application of the influence lines}
The principal benefit of influence lines is the easy detection of
load distributions which lead to extreme values of the respective
quantities. A uniform continuous loading can be considered in the
last example of truss which can be distributed arbitrarily on the
upper chord. Without influence line it is not evident which
\index{chord}%
distribution renders the extreme member force $N_c$.\\ \\
\setfigwidth{0.4\textwidth}
\bemifi
\figdata{infl_apl_truss.pdf}
\insiFig{0.10}{0.50}{$\eta N_c:$}%
\insFigU%
\begin{figsnum}
Extreme member force in the diagonal \label{infl_apl_truss}
\end{figsnum}
\enmifi
\becol
The shape of the influence line clearly defines the indicated
distributions of uniform loading to achieve maximum and
minimum $N_c$. The values of the extremes can be obtained by
integration analogous to equation~\refpar{eq_infl1}
\[ max~N_c = \int_a f~\eta N_c (x) dx = f~0.707\cdot 2.66~{1 \over 2}
   = f~0.942 \]
\[ min~N_c = \int_b f~\eta N_c (x) dx =-f~0.354\cdot 1.33~{1 \over 2}
   = -f~0.236 \]
The extremes could also be computed by solving the truss for the
two indicated load distributions.
\encol\\

\section{Solved examples}
\label{app_inlu}
\setfigwidth{0.4\textwidth}
\bemifi
\figdata{execinfl1.pdf}
\insFigU%
\enmifi
\begin{colfill}{20mm}
\figdata{execinfl7.pdf}
\insFigU%
\encol\\ \\ \\ \\
\bemifi
\figdata{execinfl3.pdf}
\insFigU%
\enmifi
\begin{colfill}{20mm}
\figdata{execinfl2.pdf}
\insFigU%
\encol
\newpage
\bemifi
\figdata{execinfl4.pdf}
\insFigU%
\\
Part II of the above frame is statically internally indeterminate
and internal forces cannot be determined inside the small rectangle
with two internal hinges. Outside of it, however, all forces can be 
determined from the equilibrium conditions.
\index{determinacy statical}%
\enmifi
\becol
\figdata{execinfl6.pdf}
\insFigU%
\encol\\
\\ \\ \\ \\ \\
\begin{center}
\setfigwidth{0.8\textwidth}
\figdata{execinfl5.pdf}
\insFigU%
\end{center}
\end{document}
