\documentclass[12pt]{article}
\usepackage{graphics,../my,../comb}
\usepackage{makeidx}
\graphicspath{{figures/}}
%\newlength{\figwidth}
%\newcommand{\insinFig}[3]
%   {\makebox[0pt][l]{\raisebox{-#1\figwidth}{\hspace{#2\figwidth} #3}}}
%\newcommand{\insFig}[2]
%   {\makebox[#2][l]{\resizebox{#2}{!}
%   {\rotatebox{-90}{\includegraphics{#1}}}}}

\oddsidemargin 0mm

\renewcommand{\sketch}{Sketch}
\renewcommand{\arule}{Rule}
\renewcommand\contentsname{Contents}
\renewcommand\refname{References}
\renewcommand\tablename{Table}
\usepackage{times}
\makeindex
%\flushbottom

\begin{document}
\title{STA course, exercise and homework 12, the finite element method,
CST element}
\author{Petr \v Re\v richa }
%\month{May}
%\year{1997}
%\date{}
\maketitle
%\tableofcontents

\topmargin -15mm
\topmargin -24mm
% the page size required by CVUT Edition center:
\textheight 250mm
\textwidth 160mm
\oddsidemargin 0mm
\footskip 10mm
\pagestyle{plain}
\textfloatsep 0pt
The default units used in numerical examples are $m$, $kN$, $kNm$, $kPa$.
 We thus do not indicate units in examples as a rule.
This is valid for the whole course. In design practice the units must be given
at least for the results!
\\ \\
The exercise complements sections 6.4 of the lecture notes
'FEM method'
on the course web page, just 'Notes' forthwith. They together
should substitute classes of the present 12th week of the semester.
Questions concerning the stuff, address, please, to\\
'petr.rericha@fsv.cvut.cz'.\\ 
The purpose of the eleventh week's exercise is gain a 'hands on'
experience with the simplest 2D element.
It is recommended to try to solve each of the examples step by step
on your own first
and only when at loss at some point of the solution, resort to the text.
This is how I tend to teach in contact classes. In this contactless
form it requires more selfcontrol on your part, of course. 
\\
HW 12 should be delivered at the same address by Tuesday, December 15th,
 2pm.

\subsection{A rectangular sheet meshed by two CST elements}
Three nodes of the mesh are fixed so that just two DOFs, $r_{4,x}$ and
$r_{4,y}$ remain free. The CST element has just two translational DOFs
at each node. Fixing of a node by a pin support or clamped node
makes thus no difference.
In order to be as brief as possible, the boundary conditions are
applied at the element level so that the structure stiffness matrix
is assembled as 2x2 matrix right away. 
Plane stress conditions are assumed. Unit thickness of the sheet is
assumed for simplicity. This implies that forces appearing in
equilibrium conditions and elsewhere are forces per unit thickness
of the sheet and have to be multiplied by the actual thickness to
get the true values. This applies to the loading force $F_4$, too.

Two types of load are applied, the uniform dead load $g$ in the
area of the sheet (self weight) and a pin force $F_4$ acting at
joint 4. The dead load also is given for unit thickness so that
its physical dimension is [kN/m$^2$]
but the value of it equals the weight of the material per unit
volume!
\\
\insFigUp{sa-exer12-ex1.pdf}{0.8\textwidth}
\subsubsection{Element 1, nodes 1,2,4}
Nodes $i=1,~j=2,~k=4$ and $a_1=0,~a_2=-1,~a_4=1,~b_1=-1,b_2=1,b_4=0$
define the geometry of the element. The stripped $[B]$ matrix includes
just the DOFs at node 4 (the last two columns of the 
matrix in eq.~(67) of the Notes):    %~\label{eq_fem29}:
\[ [B]= {1\over 2A}\left[\begin{array}{cc}0&0\\0&1\\1&0\end{array}\right]\]
Caution is necessary in later substitution of numbers. The units in
the $[B]$ matrix have the physical dimension [m]!
The material stiffness matrix is recalled for convenience:

It is inherent feature of the FEM that equilibrium conditions are
met only approximately. In case of the CST element, the equilibrium
conditions are met inside the element if the volume load ($g$ is the
example) is zero. The conditions are violated on all element boundaries.
It is evident on the external boundaries of the sheet, students are
recommended to check them on the boundary between the elements.
The tractions would have to cancel each other. 
 \[[D]= {E\over 1-\nu^2}  \left[  \begin{array}{ccc}
   1 & \nu & 0 \\
      & 1  & 0 \\
      && {1-\nu \over 2} 
  \end{array} \right] \]
The element stiffness matrix
\[ [K]^{1,2,4}=\int_A[B]^T[D][B]\dd A={E\over 4 A(1-\nu^2)}
\left[\begin{array}{cc}{1-\nu\over 2}&0\\ 0&1\end{array}\right]\]
The physical dimension of the entries in the matrix is [m$^2$]!
The element load vector $F^{1,2,4}$ owing to dead load $g$ is the 
contribution of the element to the global load vector according to
eq.~(62) in the Notes. Vector $\{f\}=\{0,g\}^T$ in the whole sheet.
The shape functions vector $[h]$, see eq.~(66)
in the Notes, can also be stripped of the joints 1 and 2 columns
\[ [h]^{1,2,4}= \left[\begin{array}{cc}h_4&0\\0&h_4\end{array}\right]\]
The element contribution to the load vector then is
$\{F\}^{1,2,4}=\int_A[h]^{1,2,4}\{f\}\dd A=\{0,gA/3\}^T$ since integral
of each shape function over the element area is the same, $A/3$,
see section 'Constant strain triangle' in the Notes.

\subsubsection{Element 1, nodes 1,4,3}
Nodes $i=1,~j=4,~k=3$ and $a_1=-1,~a_4=0,~a_3=1,~b_1=0,b_4=1,b_3=-1$
define the geometry of the element. The stripped $[B]$ matrix includes
just the DOFs at node 4 (the central two columns of the 
matrix in eq.~(66) of the Notes):    %~\label{eq_fem29}:
\[ [B]= {1\over 2A}\left[\begin{array}{cc}1&0\\0&0\\0&1\end{array}\right]\]
The element stiffness matrix
\[ [K]^{1,4,3}=\int_A[B]^T[D][B]\dd A={E\over 4 A(1-\nu^2)}
\left[\begin{array}{cc}1&0\\ 0&{1-\nu\over 2}\end{array}\right]\]
\[ [h]^{1,4,3}= \left[\begin{array}{cc}h_4&0\\0&h_4\end{array}\right]\]
$\{F\}^{1,4,3}=\int_A[h]^{1,4,3}\{f\}\dd A=\{0,gA/3\}^T$

\subsubsection{Global equilibrium}
Allocation of the element contributions in the global stiffness matrix
and load vector is trivial in this simple case.
The pin force at node 4 is not included in the expression
$\{F\}=\int_{\Omega}[h]\{f\}\dd \Omega$ as it can be read in equation (62)
of the Notes. The expression includes just the volume load $\{f\}$
since the load acting on the element boundary was formally removed
on the  transition form eq. (42) to (43) in the Notes for the sake of
brevity. Equation (42) indicates that the work of forces acting
at the boundaries of elements on the virtual displacements of the
boundaries associated with individual DOFs 
is to be added to the global load vector.
Force $F_4$ does work on $\delta r_{4,y}$ so that the contribution
to the global load vector is $\{0,F_4\}^T$.

% Caution deserves
%the pin force. In analogy to the techniques used in the matrix
%displacement method for planar frames, the FEM equilibrium 
%equations sum up forces acting upon the elements. The force appears
%thus with negative sign in the load vector.
\[ {E\over 4 A(1-\nu^2)}
\left[\begin{array}{cc}1+{1-\nu\over 2}&0\\ 0&1+{1-\nu\over 2}
 \end{array}\right] \left\{\begin{array}{c} r_{4,x}\\r_{4,y}
\end{array}\right\}
 =\left\{\begin{array}{c}0\\{2gA\over 3}+F_4\end{array}\right\} \]
\[\Rightarrow~~ \{r\}_4= {8A(1-\nu^2)\over E(3-\nu)}
 \left({2gA\over 3}+F_4\right)
 \left\{\begin{array}{c}0\\1\end{array}\right\} \]
Units in the stiffness matrix have physical dimension [m$^2$] therefore
the units in the rightmost vector have physical dimension [m$^{-2}$].
It is a good habit to perform a dimensional check of similar results. 
The  physical dimension of the result is (using basic units)
 [${m^2 m^2 N \over N m^2 m}=m$, which is correct since the left
side are displacements.
Recall that $A$ is the area of the element $A=0.5$ and the default
positive sense of $g$ and $F_4$ is in the positive sense of the
$y$ axis - upwards.

\subsubsection{Stresses}
Stress in the $1,2,4$ element 
\[ \{\sigma\}= [D][B]\{r\}=
{E\over 1-\nu^2}  \left[  \begin{array}{ccc}
   1 & \nu & 0 \\ & 1  & 0 \\ && {1-\nu \over 2} \end{array} \right]
 {1\over 2A}\left[\begin{array}{cc}0&0\\0&1\\1&0\end{array}\right]
{8A(1-\nu^2)\over E(3-\nu)}\left({2gA\over 3}+F_4\right)
 \left\{\begin{array}{c}0\\1\end{array}\right\} \]
\[\left\{\begin{array}{c}\sigma_{xx}\\\sigma_{yy}\\\sigma_{xy}
  \end{array}\right\}=
 {4\over 3-\nu}\left({2gA\over 3}+F_4\right)
\left\{\begin{array}{c}\nu\\1\\0\end{array}\right\} \]
Stress in the $1,4,3$ element
\[\left\{\begin{array}{c}\sigma_{xx}\\\sigma_{yy}\\\sigma_{xy}
  \end{array}\right\}=
 {4\over 3-\nu}\left({2gA\over 3}+F_4\right)
\left\{\begin{array}{c}0\\0\\{1-\nu\over 2}\end{array}\right\} \]
There are two normal stresses and no shear stress in the $x-y$ system
in the first element which means that they are the principal stresses.
There is just the shear stress in element $1,4,3$ which means the
principal stresses are inclined at $45^o$ to $x$ axis.
The ratios and directions of the prinicipal stresses for
$\nu=0.3$ (usual for steel) are indicated in the right sketch of the
above figure. Positive (upwards) $F_4$ and $g$ were assumed. If
the loads are negative, the indexes of the principal stresses will
change.
\\ \\
HW12:\\
Compute stress tensor and draw the principal stresses in elements
in the thin sheet meshed in two CST elements.
$g=-25~{\rm kN/m^3},~~E=1,~~\nu =0 $.
\\
\insFigUp{sa-exer12-hw.pdf}{0.5\textwidth}
\\
\newpage
HW12 solution:
\\
Two DOFs remain free, $\{r\}_4$. When the fixed DOFs are skipped in all
matrices, just $a$a and $b$s of node 4 are necessary and are the same
for both elements:
\[ a_4=0,~~b_4=2,~~A=10,~~~[B]={1\over 20}
\left[\begin{array}{cc}2&0\\0&0\\0&2 \end{array}\right] \]
\[[D]=\left[\begin{array}{ccc}1&0&0\\0&1&0\\0&0&0.5\end{array}\right],~~~
[K]^{1,4,2}=[K]^{2,4,3}=\int_A[B]^T[D][B]\dd A=10[B]^T[D][B]=
\left[\begin{array}{cc}0.1&0\\0&0.05 \end{array}\right]\]
\[ [K]=\left[\begin{array}{cc}0.2&0\\0&0.1\end{array}\right],~~~
\{F\}^{1,4,2}=\{F\}^{2,4,3}=
\left\{\begin{array}{c}0\\gA/3\end{array}\right\} =
\left\{\begin{array}{c}0\\-83.3\end{array}\right\} \]
\[ \{F\}=2\{F\}^{1,4,2}+\left\{\begin{array}{c}0\\100\end{array}\right\}=
 \left\{\begin{array}{c}0\\-66.6\end{array}\right\}\]
\[ \{r\}=\left\{\begin{array}{c}0\\-666\end{array}\right\},~~~
[\sigma_{1,4,2}\}=\sigma_{2,4,3}= [D][B] \{r\}=
\left\{\begin{array}{c}0\\0\\-33\end{array}\right\} \]
In the $x-y$ coordinates the stress is pure shear. The principal
stresses equal each other but for signs and are inclined $\pm 45^o$
from the $x$ axis.
The algorihm in sec.~(2.4), part on the plane stress
of the Notes, determines the sign as negative. 
\\
\insFigUp{sa-exer12-hw-sol.pdf}{\textwidth}
\end{document}
