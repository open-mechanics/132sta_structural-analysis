\section{Appendix - some vector and tensor algebra}
\label{\appendixlabel}
In direct notation, vectors/tensors are lower/upper case bold letters
in this appendix.
In matrix form,
column matrices (vectors in common language)/square matrices are
indicated by braces/square brackets ($\{\}/[]$).
When possible, both notations are presented alongside.
Outside the appendix, tensors may have lower case letters,
in particular stress and strain tensors $\bold{\sigma}$ and
$\bold{\varepsilon}$ since it is a long time tradition.
\\
The cartesian components $\{a\}=
\left\{\begin{array}{c}a_x \\ a_y \end{array}\right\}$
 of a vector $\bold{a}$ 
and the sum $\bold{a}+\bold{b}$, $\{a+b\}=\{a\}+\{b\}$, of two vectors
are best defined in graphic representation in~\sketch~\ref{\vectorslabel}.
The length of a vector $|\bold{a}|$ can be computed via cartesian
components, $|\bold{a}|= \sqrt{a_x^2+a_y^2}$. 
The scalar product of two vectors $\bold{a\cdot b}$ is a scalar (number)
\[ \bold{a\cdot b}= |\bold{a}||\bold{b}|\cos{\alpha} = [a]^T[b]=[b]^T[a]\]
Note that vector order can be reversed in the scalar product
and further
\[\bold{a\cdot b}= 0~~~for~~~ \bold{a}\perp \bold{b}\]
\[ \bold{a\cdot b}= 
 |\bold{a}| |\bold{b}|~~~for~~~\bold{a}\parallel \bold{b}. \]
\begin{figure}
\setfigwidth{0.9\textwidth}
\begin{center}
\insFigUp{vectors.pdf}{\figwidth}
\caption{Vectors, cartesian components, sum and scalar product}
\label{\vectorslabel}
\end{center}
\end{figure}
$\bold{C}$ is the sum of tensors $\bold{C}=\bold{A}+\bold{B}$ when
\[ \bold{C} \bold{x}= \bold{A}\bold{x}+\bold{B}\bold{x}~~~~\forall \bold{x}
    ,~~~~[C]= [A]+[B]
\]
Tensor $\bold{A}^T$ is transposed to $\bold{A}$ when
\[ \bold{a}\bold{\cdot}\bold{A}^T  \bold{b} =
   \bold{b}\bold{\cdot}\bold{A} \bold{a} ~~~~\forall \bold{a}, \bold{b},
   ~~~~[A]^T
\] 
$\bold{A}$ is a symmetric tensor when $\bold{A}^T= \bold{A}$. A symmetric
tensor has a symmetric matrix.\\
Symmetric part $\bold{A}_S$ of any general tensor $\bold{A}$ is
\[ \bold{A}_S= 0.5(\bold{A}+\bold{A}^T)~~~~[A_S]=0.5([A]+[A]^T), \]
its antisymmetric part $\bold{A}_A$ is
\[ \bold{A}_A= 0.5(\bold{A}-\bold{A}^T)~~~~[A_A]=0.5([A]-[A]^T), \]
Apparently
\[ \bold{A}_S + \bold{A}_A = \bold{A} \]

\subsection{Vector and tensor fields}
\label{\fieldslabel}
Scalar, vector and tensor fields occur in the structural analysis 
of general bodies. The variables vary inside the volume $\Omega$ and
boundary  $\Pi$ of a body with the position vector $\bold{x}$. 
The instruments and notation necessary for
differentiation and integration such fields with respect to $\bold{x}$
are briefly introduced here. The differentiation with respect to $\bold{x}$
is called the gradient forthwith.
In direct notation, the gradient operator is $\bold{\nabla}$.
Gradient of a scalar field like temperature $t$ results in a vector
field of the temperature gradient $\bold{\nabla}t$.
Gradient of a vector field like displacement $\bold{u}$ yields
a tensor field $\bold{\nabla u}$. 
In matrix notation, symbolic matrix products can be used,
$\{\partial \} t$ and $\{\partial \} \{u\}^T$, with 
$\{\partial \}= \{\partial / \partial x , \partial / \partial y,
 \partial / \partial y \}^T$ for 3D bodies. 
The elements of the operator matrix $\{\partial \} $ are applied to,
not multiplied by,
the scalar $t$ or displacement components matrix $\{u\}$.
Gradient of a tensor field of stress is necessary for differential volume
equilibrium equations. 
It yields a tensor of third order $\bold{\nabla \sigma}$
which specifies the rate of change of the stress tensor at $\bold{x}$.
In equilibrium equations of a differential volume just the vector of total
force is necessary that acts upon the surface of the volume. The vector
is obtained by contraction (or partial trace) of $\bold{\nabla \sigma}$. 
It is convenient to define divergence $div$,
an operator which performs gradient and contraction consecutively. When
applied to the stress tensor field, it yields the vector of total
force  that acts upon the surface of the differential volume,
${\rm div} \bold{\sigma}$.
Divergence of a tensor field $\bold{\sigma}$, ${\rm div}\bold{\sigma}$, 
is difficult to express in matrix notation when tensor components are in
a square matrix. It is easy when they are set in a column matrix
$\{\sigma \}$, see expression~\refpar{eq_fem21}. Then the components
of ${\rm div}\bold{\sigma}$ are obtained as $[\partial ]^T \{\sigma \}$
where the operator $[\partial ]$ is specified for 3D bodies
in ~\refpar{eq_fem23}.

\subsection{Orthogonal transformation,  invariance of $\sum \sigma_{ii}$}
\label{\ortholabel}
Consider a pair of orthonormal bases vectors $\{\bold{n}\}$ and
$\{\bold{n'}\}$ of two cartesian coordinate systems.
In plane problems, for instance, $\{\bold{n}\}^T= 
\{\bold{n}_x,\bold{n}_y\}$.
Vector $\bold{n'}_x$ can be expressed in terms of the
{ $\{\bold{n}\}$ basis using the projections of it on $\{\bold{n}\}$
vectors:
\[ \bold{n'}_x= (\bold{n'_x} \bold{\cdot} \{\bold{n}\}^T) \{\bold{n}\}  \]
The same can be done for all vectors of $\{\bold{n'}\}$:
\[ \{\bold{n'}\}= (\{\bold{n'} \}  \bold{\cdot} \{\bold{n}\}^T) 
 \{\bold{n}\}= [T] \{\bold{n}\}  \]
where the transformation matrix $[T]$,
\[ [T]=  \{\bold{n'} \}  \bold{\cdot} \{\bold{n}\}^T ,  \]
is a matrix of scalar factors (direction cosines).
The reverse transformation matrix turns out to be transpose $[T]$:
\[ \{\bold{n} \}  \bold{\cdot} \{\bold{n'}\}^T = [T]^T \]
The orthogonality of $[T]$ can be shown by substituting for $\{\bold{n}\}$
from the other of these two expressions:
\[ \{\bold{n'}\} =[T] \{\bold{n}\},~~~~ \{\bold{n}\} =[T]^T \{\bold{n'}\},\]
The resulting equation
\[ \{\bold{n'}\} [T][T]^T \{\bold{n'}\} \]
must be met for arbitary orthonormal basis $\{\bold{n'}\}$ which implies
\[ [T]^T = [T]^{-1} \]
Transformation matrix $[T]$ applies to components $\{a\}$ of
 a general vector $\bold{a}$, too. It is apparent when the vector is
written in terms of two cartesian bases,
\[ \bold{a}= \{a\}^T\{\bold{n}\},~~~~\bold{a}= \{a'\}^T\{\bold{n'}\}, \]
and next it is substituted for $\bold{n}$ from the second of the above 
expressions for the bases tranformations. It follows that
\[ \{a'\}= [T] \{a\} \]
It is now possible to show that the sum of diagonal elements
of the stress tensor in any cartesian system equals
 $I_1=\sigma_1+\sigma_2+\sigma_3$ and is thus invariant with respect to
the coordinate system.
When $\{\bold{n}\}$ are the principal directions vectors of a stress
tensor $\bold{\sigma}$, then
\[ I_1= \{\bold{n}\}^T \bold{\cdot} \bold{\sigma} \{ \bold{n} \} \]
Let $[T]$ be the transformation matrix from $\{ \bold{n}\}$ to 
an arbitrary basis  $\{ \bold{n_x}\}$. Then
\[ I_1= \{ \bold{n_x}\}^T [T] \bold{\cdot} \bold{\sigma} 
  [T]^T   \{ \bold{n_x}\}= \{ \bold{n_x}\}^T  \bold{\cdot} \bold{\sigma}
\{ \bold{n_x}\}=
\sigma_{xx}+\sigma_{yy}+\sigma_{zz} \]
