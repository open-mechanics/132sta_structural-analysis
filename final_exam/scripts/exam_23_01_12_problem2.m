clc; % clears text from Command Window
clear all; % clears the workspace 
close all; % close all figure windows

%% Input parameter
E = 200 * 1e9; % Youngs modulus in Pa
A = 6.45e-3; % cross section area in m^2
I = 125e-6; % geometrical moment of inertia in m^4

L1 = 3; % length of element 1 in m
L2 = 6; % length of element 2 in m
Coo = [0 0; 0 3; 6 3]

%% Element stiffness matrices

% Transformation matrix
% Member 1
lamx = (Coo(2,1)-Coo(1,1))/L1;
lamy = (Coo(2,2)-Coo(1,2))/L1;
k1 = stiffmat(E,A,I,L1,lamx,lamy)

% Member 2
lamx = (Coo(3,1)-Coo(2,1))/L2;
lamy = (Coo(3,2)-Coo(2,2))/L2;
k2 = stiffmat(E,A,I,L2,lamx,lamy)

%% Global stiffness matrix

K = zeros(9,9);

k = zeros(6,6,2); % defining a 6x6x3 array for the local stiffness matrices
k(:,:,1) = k1; % filling the first layer with k1
k(:,:,2) = k2; % filling the second layer with k2

% table of global degrees of freedom for each element
% first row element 1, and so on
TDOF = [1 2 3 4 5 6;
        4 5 6 7 8 9 ];

for iElem = 1:2
    for iRow = 1:6
        for iCol = 1:6
            K(TDOF(iElem,iRow),TDOF(iElem,iCol)) = K(TDOF(iElem,iRow),TDOF(iElem,iCol)) + k(iRow,iCol,iElem);
        end
    end
end

%% Solving the system of equations

F = zeros(9,1); % force vector filled with zeros
F(5) = -90e3; % vertical force in N
F(6) = -90e3; % moment in Nm
F(8) = -90e3; % vertical force in N
F(9) =  90e3; % moment in Nm

D = zeros(9,1); % displacement vector filled with zeros

D([3,4,5,6,7,9]) = K([3,4,5,6,7,9],[3,4,5,6,7,9]) \ F([3,4,5,6,7,9])

% Calculating unkown reaction forces
F([1,2,8]) = K([1,2,8],[3,4,5,6,7,9]) * D([3,4,5,6,7,9])

% Calculating element end forces for bonus points
F2 = ( k2*D(4:9) - 90e3*[0;-1;-1;0;-1;1] ) / 1e3

%% Function to calculate element stiffness matrix in global coordinates
function k = stiffmat(E,A,I,L,lamx,lamy)
T1 = zeros(6,6);
T1(1,1) = lamx;
T1(1,2) = lamy;
T1(2,1) = -lamy;
T1(2,2) = lamx;
T1(3,3) = 1;
T1(4,4) = lamx;
T1(4,5) = lamy;
T1(5,4) = -lamy;
T1(5,5) = lamx;
T1(6,6) = 1;

k_prime = zeros(6,6);
k_prime(1,1) = A*E/L;
k_prime(1,4) = -A*E/L;
k_prime(4,1) = -A*E/L;
k_prime(4,4) = A*E/L;

k_prime(2,2) = 12*E*I/L^3;
k_prime(2,5) = -12*E*I/L^3;
k_prime(5,2) = -12*E*I/L^3;
k_prime(5,5) = 12*E*I/L^3;

k_prime(2,3) = 6*E*I/L^2;
k_prime(2,6) = 6*E*I/L^2;
k_prime(5,3) = -6*E*I/L^2;
k_prime(5,6) = -6*E*I/L^2;

k_prime(3,2) = 6*E*I/L^2;
k_prime(6,2) = 6*E*I/L^2;
k_prime(3,5) = -6*E*I/L^2;
k_prime(6,5) = -6*E*I/L^2;

k_prime(3,3) = 4*E*I/L;
k_prime(6,6) = 4*E*I/L;
k_prime(6,3) = 2*E*I/L;
k_prime(3,6) = 2*E*I/L;


k = T1'*k_prime*T1;
end
