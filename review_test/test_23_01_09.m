function test_23_01_09

EA = 400e6; % From the assignment

K1 = stiffness_matrix( EA / 4.5, 0.8, -0.6 ) % Element #1
K2 = stiffness_matrix( EA / 2.7,   0,   -1 ) % Element #2

%% Assembly of stiffness matrix
K = zeros(6,6);

K([1:4], [1:4]) = K([1:4], [1:4]) + K1;
K([3:6], [3:6]) = K([3:6], [3:6]) + K2

%% Column of nodal forrces
f = zeros(6,1);

f(3) = -5e3;

%% System solve
d = zeros(6,1);
u = [3]; % Unkowns
k = [1,2,4,5,6]; % Knowns

d(u) = K(u,u) \ f(u) % Solve for uknown displacements
f(k) = K(k,:) * d % Extract reactions

%% Internal forces
N1 = internal_forces( EA / 2.7, 0.8, 0.6, d( [1, 2, 3, 4] ) )
N2 = internal_forces( EA / 4.5,   0,  -1, d( [3, 4, 5, 6] ) )

    function Ke = stiffness_matrix( n, c, s ) % Membrane stiffness matrix  
        Ke = n * ...
            [ c*c,  c*s, -c*c, -c*s; 
              c*s,  s*s, -c*s, -s*s;
             -c*c, -c*s,  c*c,  c*s; 
             -c*s, -s*s,  c*s,  s*s ]; 
    end

    function Ne = internal_forces( n, c, s, de )
        Ne = n * [-c, -s, c, s ] * de;
    end

end