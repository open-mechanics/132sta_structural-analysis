#
# Factors two, three and six assume values
#          2     3        6          for both ends attached
#          1.5  1.5      1.5         for joint j hinge connected
# in which case if (two==1.5) is used to identify the hinge
# connection!!!
# 
#
global two three six;
two=2;three=3;six=6;
function klocii=klocii(ax,k,l)
 global two three six;
 klocii=[ax,0,0;0,six*k/l^2,three*k/l;0,three*k/l,two*k];
 return
endfunction
function klocjj=klocjj(ax,k,l)
 global two three six;
 klocjj=[ax,0,0;0,six*k/l^2,-three*k/l;0,-three*k/l,two*k];
# hinge at j:
 if (two==1.5)
 klocjj=[ax,0,0;0,six*k/l^2,0;0,0,0];
 endif
 return
endfunction
function klocij=klocij(ax,k,l)
 global two three six;
 klocij=[-ax,0,0;0,-six*k/l^2, three*k/l;0,-three*k/l,k];
# hinge at j:
 if (two==1.5)
 klocij=[-ax,0,0;0,-six*k/l^2,0;0,-three*k/l,0];
 endif
 return
endfunction
function tram=tra(cal,sal)
 tram=[cal,sal,0;-sal,cal,0;0,0,1];
 return
endfunction
# used in class 12/10/11: 
#Example 1: 2 clamped - horizontal 8m- 3 -horizontal 6m - 4 vert. sliding 
#                        1 clamped, 1,3 inclined 5m long attached to 3 
#           |2-------------3--------4|| 
#                        / 
#                       / 
#                      / 
#                    _1_
#                    
printf("Example 1, 2 hor. beams, 1 inclined leg \n");
EJ=120;EA=10;l23=8;l13=5;l34=6;
k13=2*EJ/l13
k23=2*EJ/l23
k34=2*EJ/l34
a13=EA/l13
a23=EA/l23
a34=EA/l34
kloc23=klocjj(a23,k23,l23)
kloc34=klocii(a34,k34,l34)
kloc13=klocjj(a13,k13,l13)
t13=tra(0.8,0.6);
k13g=t13'*kloc13*t13
kg=k13g+kloc23+kloc34
kgfull=horzcat(vertcat(kg,[0,-six*k34/l34^2,-three*k34/l34]),[0;-six*k34/l34^2;-three*k34/l34;six*k34/l34^2])
F=[0;0;0;100]
u=kgfull\F
ucut=[u(1);u(2);u(3)];
S32=kloc23*ucut
S31=kloc13*t13*ucut
kg34full=horzcat(vertcat(kloc34,[0,-six*k34/l34^2,-three*k34/l34]),[0;-six*k34/l34^2;-three*k34/l34;six*k34/l34^2])
S34=kg34full*u
#
# Example 1 modified, slide support at joint 3:
#           |-----------3||
#                      /
#                     /
#                    /
#                  _1_
#                 
printf("Example 1 modified, vert. slide support at 3 \n");
mk32=kloc23(2,2)
mk31=k13g(2,2)
ucut=[0; -100/(mk32+mk31);0]
S32=kloc23*ucut
S31=kloc13*t13*ucut
# Example 1 modified twice now hinge at 3
printf("Example 1 modified twice, hinge at 1 \n");
# a single DOF v_3
two=1.5;three=1.5;six=1.5;
k3133=klocii(a13,k13,l13)
t31=tra(-0.8,-0.6)
k3133g=t31'*k3133*t31
kg= kloc23(2,2)+ k3133g(2,2)
v_3= -100/kg
u3= [0,v_3,0]'
S32= kloc23*u3
u3l31= t31*u3
S31=k3133*u3l31 
#
# Example 1 modified, vert. roller at joint 3, 2 DOFs:
#         2 |-----------3<||
#                      /
#                     /
#                    o
#                  _1_
#                 
printf("Example 1 modified vert. roller at joint 3, hinge at 1 \n");
 two=2;three=3;six=6;
 k2333=klocjj(a23,k23,l23)
 k2323=klocij(a23,k23,l23)
 two=1.5;three=1.5;six=1.5;
 k3133l=klocii(a13,k13,l13)
 t31=tra(-0.8,-0.6)
 k3133g=t31'*k3133l*t31
printf("1st DOF, x transl. is constrained - row and column 1 omitted \n");
 k=k2333+k3133g
 kred=[k(2,2),k(2,3);k(2,3),k(3,3)]
 f=[4;-l23^2/12]
 ured=-kred\f
 u3=[0;ured(1);ured(2)]
 s32displ=k2333*u3
 s32=s32displ+[0;f(1);f(2)]
 s23disp=k2323*u3
 s23=s23disp+[0;f(1);-f(2)]
 u3l31=t31*u3
 s31=k3133l*u3l31
# 
#
# Example 3, girder supported by a link
#         |----------------------|  --
#         1        2 o           3
#                      \
#                        \
#                          \         5
#                            \
#                              \
#                              4 o  --
#
printf("Example 3, beam supported by a link \n");
EJ=12000
EA=300000
k12=2*EJ/4
a12=EA/4
l24=sqrt(4^2+5^2)
a24=EA/l24
two=2;three=3;six=6;
k12l=klocjj(a12,k12,4)
k23l=klocii(a12,k12,4)
k24l=klocii(a24,0,l24)
co=4/l24
si=-5/l24
t=tra(co,si)
k24g=t'*k24l*t
kg=k12l+k23l+k24g
load=[0;2;-1.33]
u=-kg\load
s21displ=k12l*u
s21=s21displ+load
k1212l=klocij(a12,k12,4)
s12displ=k1212l*u
s12= s12displ+[0;2;1.333]
s23=k23l*u
s32=k1212l'*u
s24=k24l*t'*u

#
# Example 4 two hinge roof, 2+2 times 1.5 m
#                      3        ---
#                    /   \       |
#                   /     \   1.5|
#                  /       \     |
#                 o         o   ---
#                 1         2
#                 |----4----|
#
k=0.4;a=2.5;l=2.5;
printf("Example 4 two hinge roof, rotation fi1 included 4x4 matrix\n");
printf("Local x from 3 (the top) to 1 (left hinge)\n");
printf("4DOF are retained (u3x,u3y,fi3,fi1)");
printf("the last line of k3131 is the last line of the 4x4 matrix\n");
k3133l=klocii(a,k,l)
k3131l=klocij(a,k,l)'
k3133lcond=zeros(3);
printf("Three steps of condensation:\n");
k3133lcond(1,:)= k3133l(1,:)- k3131l(3,:)*k3131l(3,1)/0.8
k3133lcond(2,:)= k3133l(2,:)- k3131l(3,:)*k3131l(3,2)/0.8
k3133lcond(3,:)= k3133l(3,:)- k3131l(3,:)*k3131l(3,3)/0.8
printf("Note that the above condensed matrix is the same\n");
printf("as the one obtained below through modified K3133\n");
trmat=tra(-4/5,-3/5);
trmat31=trmat
k3133g=trmat'*k3133l*trmat
k3131g=trmat'*k3131l*trmat
# second, fi1 is condensed out first
#
two=1.5;three=1.5;six=1.5;
printf("With modified stiffness matrix:\n");
k3133l=klocii(a,k,l)
k3133g=trmat'*k3133l*trmat
printf("The k3133l can be used for beam 1,2 as well\n");
printf("Transf. matrix is different\n");
trmat=tra(4/5,-3/5)
k3233g=trmat'*k3133l*trmat
kg=k3133g+k3233g
printf("load vector\n");
load=[-1;0;0]
printf("displacements\n");
u=kg\load
s31l=k3133l*trmat31*u
s32l=k3133l*trmat*u
#
# Example 4a clamped roof, 2+2 times 1.5 m
#                      3        ---
#                    /   \       |
#                   /     \   1.5|
#                  /       \     |
#                 -         -   ---
#                 1         2
#                 |----4----|
#
two=2;three=3;six=6;
k=0.4;a=2.5;l=2.5;
printf("Example 4 clamped roof, 3 DOFs\n");
printf("Local x from 1 to 3(the top) and from 2 to 3\n");
Kl1333=klocjj(a,k,l)
Kl1333=[2.5,0,0;0,0.384,-0.48;0,-0.48,0.8]
T13=[0.8,0.6,0;-0.6,0.8,0;0,0,1]
Kg1333= T13'*Kl1333*T13
T23=[-0.8,0.6,0;-0.6,-0.8,0;0,0,1]
Kg2333=T23'*Kl1333*T23
F=[-1;0;0]
K=Kg1333+Kg2333
u=-K\F
S31=Kl1333*T13*u
S32=Kl1333*T23*u
#
# Example 5
# exam task, beam 10m long, inclined by 30deg from hor. EA=10000,EJ=10000,
#
printf("Example 5,beam 10m long, inclined by 30deg from hor.\n");
printf("EA=10000,EJ=10000");
two=2;three=3;six=6;
u1g=[-0.02;0.04;-0.006];
u2g=[0.01;-0.012;-0.005];
a=1000;k=2000;l=10;
kloc11=klocii(a,k,l)
kloc22=klocjj(a,k,l)
kloc12=klocij(a,k,l)
t=tra(sqrt(3)/2,1/2)
u1l=t*u1g
u2l=t*u2g
s1l=kloc11*u1l+kloc12*u2l
s2l=kloc12'*u1l+kloc22*u2l
#
# Example 6
# cantilever of two beams: beam 1,3 horizontal 12 m, beam 2,3 inclined
# the left end 5m lower and 13 m long, uniform load 5 on beam 1,3
printf("Example 6, cantilever of two beams 12 and 13 m long");
EJ=60; EA=12;
l13=12
a13=EA/l13
k13=2*EJ/l13
l23=13
a23=EA/l23
k23=2*EJ/l23
k1333=klocjj(a13,k13,l13)
k2333l=klocjj(a23,k23,l23)
s23=5/13
c23=12/13
t=tra(c23,s23)
k2333g=t'*k2333l*t
kg=k1333+k2333g
load=[0;30;-60]
u33=-kg\load
S31=k1333*u33
u33l=t*u33
S32= k2333l*u33l
k1313=klocij(a13,k13,l13)
S13=k1313*u33
k2323l=klocij(a23,k23,l23)
S23=k2323l*u33l
#tip load
printf("displacements load at the tip 3 ************** \n");
u33= [0.277;-3.46;-0.407]
S31=k1333*u33
u33l=t*u33
S32= k2333l*u33l
#the same but rotated 90deg and given displ. at 3, xg horizontal
printf("Example 6 modified, rotated 90, xg horizontal, given {u}33 \n");
t90=tra(0,1)
k1333rot=t90'*k1333*t90
k2333lrot=t90'*k2333l*t90
k2333grot=t90'*k2333g*t90
u33=[3.46;0.277;-0.407]
S31=k1333*t90*u33
t23tot=t90*t
u33l=t90*t*u33
S32= k2333l*u33l
#the same as example 6 but rectangular cross-section 0.3x0.5m, E=1000
printf("Example 6 modified with cross-section 0.3x0.5m");
EJ=1000*0.3*0.5^3/12
EA= 1000*0.3*0.5
l13=12
a13=EA/l13
k13=2*EJ/l13
l23=13
a23=EA/l23
k23=2*EJ/l23
k1333=klocjj(a13,k13,l13)
k2333l=klocjj(a23,k23,l23)
s23=5/13
c23=12/13
t=tra(c23,s23)
k2333g=t'*k2333l*t
kg=k1333+k2333g
load=[0;30;-60]
u33=-kg\load
S31=k1333*u33
u33l=t*u33
S32= k2333l*u33l
k1313=klocij(a13,k13,l13)
S13=k1313*u33
k2323l=klocij(a23,k23,l23)
S23=k2323l*u33l
#example 7 - inclined beam 5,10, 4m hor. 3m vert. uniform load intensity 1
# implies 0.8 and 0.6 intensities lateral and axial,
# imposed joint displacements
printf("Example 7 beam 5,10, imposed joint displ. + uniform load\n");
ax=2;
k=0.4;
l=5;
u5=[1;2;0.5]
u10=[1;1;-1]
t=tra(0.8,-0.6)
u5l=t*u5
u10l=t*u10
k55l=klocii(ax,k,5)
k1010l=klocjj(ax,k,5)
k510l=klocij(ax,k,5)
s5=k55l*u5l+k510l*u10l
s10=k510l'*u5l+k1010l*u10l
#fixed end forces in local coord. system
F5=[-0.6*l/2;0.8*l/2;0.8*l^2/12]
F10=[-0.6*l/2;0.8*l/2;-0.8*l^2/12]
#total end forces
s5=s5+F5
s10=s10+F10
# example 7 modified, hinge at joint 10
printf("example 7 modified, hinge at joint 10\n");
two=1.5;three=1.5;six=1.5;
k55l=klocii(ax,k,5)
k1010l=klocjj(ax,k,5)
k510l=klocij(ax,k,5)
s5=k55l*u5l+k510l*u10l
s10=k510l'*u5l+k1010l*u10l
#fixed end forces in local coord. system
F5=[-0.6*l/2;0.8*l/2+0.8*l/8;0.8*l^2/12*1.5]
F10=[-0.6*l/2;0.8*l/2-0.8*l/8;0]
#total end forces
s5=s5+F5
s10=s10+F10
#
#    ||||||||||
# 2 |----------o 3
#           /
#       /
# 1 o
printf("example 8, cantilever 2,3 supported by an inclined link 1,3\n");
k23=25;a23=5;a13=3.6;
two=1.5;three=1.5;six=1.5;
k2333=klocjj(a23,k23,4)
k1333=klocjj(a13,0,5)
tm=tra(0.8,0.6)
k= tm'*k1333*tm
k=k + k2333
# k is singular, zero 3rd row/column, add 1 in (3,3)
k(3,3)=1
F32=[0;2-4^2/8/4;0]
u=-k\F32
s32=k2333*u
s32=s32+F32
s31=k1333*tm*u
#
#  1 o
#       \
#           \ 
#  2 |---------o 3
#        \|/
printf("example 8 modif., cantilever 2,3 suspended by link 1,3\n");
tm= tra(0.8,-0.6)
k=tm'*k1333*tm
k=k+k2333
# k is singular, zero 3rd row/column, add 1 in (3,3)
k(3,3)=1
F32= [0;0.5-1/8*4/4;0]
u=-k\F32
s32=k2333*u
s32=s32+F32
s31=k1333*tm*u
#
# HW 3 2015/16
#      3  ---------- 2
#    |-   |        o 
#  f |-   |        -
#    |-   |
#      1 ---
printf("HW 3 2015/16\n");
k=1;a=10;l13=l32=l=4;
two=2;three=3;six=6;
K1333l=klocjj(a,k,l)
T=tra(0,1)
K1333g=T'*K1333l*T
two=1.5;three=1.5;six=1.5;
K3233=klocii(a,k,l)
# because of the roller at 2, u2=u3. The 4th column can thus be addded
# to the first:
K3233(1,1)=K3233(1,1)-10
K=K3233+K1333g
Sf31=[-2;0;-1.33]
u=-K\Sf31
S31d=K1333l*T*u
S31=S31d+Sf31
S32=K3233*u
