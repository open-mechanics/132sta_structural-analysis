\documentclass[12pt]{article}
\usepackage{graphics,../comb}
\usepackage{makeidx}
\graphicspath{{figures/}}
%\newlength{\figwidth}
%\newcommand{\insinFig}[3]
%   {\makebox[0pt][l]{\raisebox{-#1\figwidth}{\hspace{#2\figwidth} #3}}}
%\newcommand{\insFig}[2]
%   {\makebox[#2][l]{\resizebox{#2}{!}
%   {\rotatebox{-90}{\includegraphics{#1}}}}}

\oddsidemargin 0mm

\newcommand{\sketch}{Sketch}
\newcommand{\arule}{Rule}
\renewcommand\contentsname{Contents}
\renewcommand\refname{References}
\renewcommand\tablename{Table}
\usepackage{times}
\makeindex
%\flushbottom

\begin{document}
\title{STA course, exercise and homework 2}
\author{Petr \v Re\v richa }
%\month{May}
%\year{1997}
%\date{}
\maketitle
%\tableofcontents

\topmargin -15mm
\topmargin -24mm
% the page size required by CVUT Edition center:
\textheight 250mm
\textwidth 160mm
\oddsidemargin 0mm
\footskip 10mm
\pagestyle{plain}
\textfloatsep 0pt
The exercise complements sections 2.1 - 2.2  of the lecture notes
'Structural analysis - displacement method for planar frames
and gridworks' on the course web page, just 'Notes'
forthwith. They together
should substitute classes of the present 2nd week of the semester.
Questions concerning the stuff, address, please, to\\
'petr.rericha@fsv.cvut.cz'.\\ 
The purpose of the second week classes is 1) assembly of
individual beams stiffness matrices and load vectors including the
boundary conditions (supports and other constraints)
2) coordinate transformations and assembly of the global stiffness matrix and
load vector in simple examples
3) back substitution for the beam end forces and drawing the internal forces
diagrams
HW 1 should be delivered at the same address
by tuesday, October 6th. If possible, process the homework
in a document processor (Word, Latex,..), scanned manuscripts are
acceptable, too. In the latter case, use reasonable resolution so that
the file is not larger than 2MB. pdf, jpg and png formats are accepted.

\subsection{Equilibrium of the structure}
With the aid of the beam stiffness matrices and load vectors, 
the equilibrium equations of the whole structure can be set up.
They are written down in the form of a matrix equation
\[ [K] \{u\}+\{F\}=\{0\} \]
Note that the left side has the same structure as the right side
of equation (4) in the Notes which delivers the end forces of
a beam. In analogy to that the left side of the above equation 
delivers the sums
of the endforces of all beams at each joint and the equation
requires that it is zero. This means that the forces acting 
at any joint are in equilibrium.
\emph{The default global coordinate system in figures of the whole
 course is the $x$ coordinate horizontal to the right, the $y$ coordinate
 vertical upwards.}

\subsection{Example 1, roof structure supports clamped }
The example is a modification of the Frame 1 in the Notes with joints 1
and 2 clamped.
It is solved here in a more straightforward manner, like the homeworks and test
tasks should be worked out. Note that other beam local coordinates are used here 
than in the Notes. The beam data are the same, repeated here for convenience.
Symmetric matrices are filled just in the upper triangle to save typing.
\\
\insFigUp{sa-exer2-ex1-frame.pdf}{0.8\textwidth}
\\
Constant $EJ$ everywhere, $E=6.52$, 
Cross-section $0.98\times 0.98$, $A=0.96$, $EJ=0.5$, $AE=6.25$\\
\[ a={EA \over l} = 2.5,~~~~~~~~k={2 EJ \over l}=  0.4 \]
DOfs of the structure are identified first. Joints 1 and 2 are clamped so that there are
just 3 DOFs at joint 3. The global stiffness matrix then is obtained as a simple sum
of the contributions of the two beams:
\[ [K]= [K]^{1,3}_{3,3,g}+[K]^{2,3}_{3,3,g} \]
\[ [K]^{1,3}_{3,3,l}=\left[\begin{array}{ccc}2.5&0&0\\&0.384&-0.48\\&&0.8\end{array}\right]
  =[K]^{2,3}_{3,3,l},~~~[T]_{1,3}=\left[\begin{array}{ccc}0.8&0.6&0\\-0.6&0.8&0\\0&0&1\\
	                          \end{array}\right]
  ,~~~[T]_{2,3}=\left[\begin{array}{ccc}-0.8&0.6&0\\-0.6&-0.8&0\\0&0&1\\
	                          \end{array}\right] \]
\[ [K]^{1,3}_{3,3,g}= [T]_{1,3}^T[K]^{1,3}_{3,3,l}[T]_{1,3}=
 \left[\begin{array}{ccc}1.74&1.02&0.288\\&1.15&-0.384\\&&0.8\end{array}\right],~~~
 [K]^{2,3}_{3,3,g}=\left[\begin{array}{ccc}1.74&-1.02&0.288\\&1.146&-0.384\\&&0.8
 \end{array}\right] \]
There is no load acting upon the beams, the global load vector includes just the
force acting at joint 3:
\[ \{F\}^T=\{-1,0,0\} \]
The minus sign in the $x$ component seems misplaced, the force acts in the direction
of the positive $x$ axis. The point is that 
in the equilibrium equations of the displacement
method as well as of the finite element method (FEM forthwith), 
the forces acting upon
the ends of the beams or elements are summed, not the forces acting upon the joint
itself. Recall that the end forces in the slope deflection and displacement methods
are the forces acting upon the ends of the beams. 
Any given external load specified as forces or moments acting direct upon
the joint(s) have to be included in the load vector with opposite sign.
The equilibrium equation reads
\[ \left[\begin{array}{ccc}3.47&0&0.576\\&2.29&0\\&&1.6\end{array}\right]\{u_3\}+
	\left\{\begin{array}{c}-1\\0\\0\end{array}\right\}=\{0\} \]
and is equivalent to three coupled linear equations for $u_3$, $v_3$ and
$\varphi_3$. The methods of solution are not the subject of the course, the most 
comfortable is the use of some matrix calculator. 

In the exam tasks two global DOFs will occur resulting in matrix equation of order 2
as a maximum. If more DOFs problem appears, the solution of the equation will be given.
In the current task, the solution is
\[ \{u\}^T=\{0.306,0,-0.110\} \]
The end forces of the beams \emph{due to joint displacements} are computed by back 
substitution, generally it is
\[ \{S\}_{i,j}^d= [K]_{i,i,l}^{i,j}[T]_{i,j}\{u\}_i+[K]_{i,j,l}^{i,j}[T]_{i,j}\{u\}_j \]
for the end forces of the beam $i,j$ at joint $i$ and
\[ \{S\}_{j,i}^d= [K]_{j,i,l}^{i,j}[T]_{i,j}\{u\}_i+[K]_{j,j,l}^{i,j}[T]_{i,j}\{u\}_j \]
for  the end forces of the beam $i,j$ at joint $j$. The same can be written
in an alternative way
\[ \left\{\begin{array}{c}\{S\}_{i,j}^d\\ \{S\}_{j,i}^d\end{array}\right\}=
 [K]^{i,j}\left[\begin{array}{cc} [T]_{i,j}&[0] \\ &[T]_{i,j}\end{array}\right]
    \left\{\begin{array}{c} \{u\}_i\\\{u\}_j\end{array}\right\}\]
This is how the step is carried out in computer codes. In the school examples
many DOFs are constrained, in the present one 
\[ \{S\}_{3,1}^d= [K]_{3,3,l}^{1,3}[T]_{1,3}\{u\}_3=
\left[\begin{array}{ccc}2.5&0&0\\&0.384&-0.48\\&&0.8\end{array}\right]
  \left[\begin{array}{ccc}0.8&0.6&0\\-0.6&0.8&0\\0&0&1 \end{array}\right]
\left\{\begin{array}{c}0.306\\0\\-0.110\end{array}\right\}=
\left\{\begin{array}{c}0.612\\-0.0176\\0\end{array}\right\}
   \]
It is required for better understanding and checks of the protocols that
the above back substitution is divided in two steps. First, the displacement
$\{u\}_3$ is explicitly transformed to the local coordinates:
\[ \{u\}_{3,1}= \left[\begin{array}{ccc}0.8&0.6&0\\-0.6&0.8&0\\0&0&1 \end{array}\right]
 \left\{\begin{array}{c}0.306\\0\\-0.110\end{array}\right\}=
\left\{\begin{array}{c}0.245\\-0.183\\-0.110\end{array}\right\}
\]
and then multiplied by the stiffness matrix in local coordinates. 
The intermediate result, $\{u\}_{3.1}$ can be thus be checked.
\emph{The second index at the joint displacement column matrix indicates that
it contains the components in the local coordinates of the respective beam.} 
There is another way to get $\{S\}_{3,1}^d$:
\[ \{S\}_{3,1}^d= [T]_{1,3}[K]_{3,3,g}\{u\}_3 \]
but it implies in most cases more numerical work since the stiffness submatrix
in global coordinates has more nonzero entries than in the local coordinates.

Beam $1,3$ is not loaded, so in this case the final end forces equal the forces
due to joint displacements alone, $\{S\}_{3,1}=\{S\}_{3,1}^d$. In later examples
(see below) when the beam is loaded the fixed end forces have to be added.

The end forces $\{S\}_{1,3}$ in beam $1,3$ can be computed the same way,
by back substitution, but once the end forces are known at one joint of a beam,
all internal forces and the end forces at the other joint can be determined
by equilibrium of the free body. All variables are in the \emph{local} coordinates.
\\
\insFigUp{sa-exer-2-ex1-freebody.pdf}{0.4\textwidth}
\\
The equil. condition to determine $M_{1,3}$:
\[ beam 1,3 \odot 3: 
 M_{1,3}+M_{3,1}+Y_{3,1}\cdot 2.5=0,~~~M_{1,3}+0+(-0.0176)\cdot2.5=0~\Rightarrow
  M_{1,3}=0.044 \]
  \emph{Equilibrium equations always should be labeled such that the label indicates
  the body and the condition - beam 1,3, moment condition with respect to joint 3
  in the above equation.}
The equilibrium of forces in the $x_l$ and $y_l$ directions yield
$X_{1,3}=-0.612$, $Y_{1,3}=0.0176$. Beam $2,3$ is treated the same way, in short,
recording only desired intermediate results, all in local coordinates,
as they would be required in a test protocol:
\[ \{u\}_{3.l}^T=\{-0.244, -0.183, -0.110\},~~~ \{S\}_{3,2}^T=\{-0.612,-0.0176,0\},
 ~~~\{S\}_{2,3}^T=\{0.612,0.0176,0\} \]
 The last step is drawing the internal forces diagrams. This is again the matter
 of judicious application of equilibrium conditions in general. There are no
 loads acting upon beams in this example, all lines in the diagrams must be
 straight lines. Care is necessary in the interpretation of the end forces,
 be aware of the different positive senses of the end forces versus internal
 forces.
\\
\insFigUp{sa-exer2-ex1-diags.pdf}{\textwidth}
\\
\subsection{Example 1 modified, uniform load on beam $1,3$}
The structure remains the same, load is a vertical uniform load upon beam $1,3$:
\\
\insFigUp{sa-exer2-ex1mod.pdf}{0.5\textwidth}
\\
The stiffness matrix is the same as in the original example since the structure
is the same. The load vector of beam $1,3$ in local coordinate system:
\[ M_{3,1}^f=-M_{1,3}^f=-1/12f\cdot 0.8 l^2= -1/12\cdot 1 \cdot 0.8 \cdot 2.5^2=-0.417\]
\[ Y_{3,1}^f=Y_{1,3}^f=-1/2.5(0.417-0.417)+0.8\cdot2.5/2=1\]
\[X_{3,1}^f=X_{1,3}^f= 0.6\cdot2.5/2=0.75 \]
Factors $0.8$ and $0.6$ are cosine and sine of the angle from the local $x_l$
axis to the direction of the load. The fixed end moments come from the table 
in the Notes, the other fixed end forces from elementary equil. conditions.
The load vector in local and global coordinates:
\[ F_{3,1,l}^f=\{0.75,1,-0.417\}^T,~~~F_{3,1,g}^f=[T]_{1,3} F_{3,1,l}^f=
   \{0,1.25,-0.417\}^T \]
There is no load beyond the load of beam $1,3$, the global load vector then
equals the load vector of beam $1,3$, $\{F\}=F_{3,1,g}^f$. The equilibrium
of the structure, $[K]\{u\}+\{F\}=\{0\}$ yields in this case
$ \{u_3\}^T=\{-0.046,-0.545,0.277\}$. The end forces of beam $1,3$ are now the sum
of the displacement induced $\{S\}_{3,1}^d$ and the fixed end forces $\{F\}_{3,1,l}$:
\[ \{S\}_{3,1}= [K]_{3,3,l}^{1,3}[T]_{1,3}\{u_3\}+\{F\}_{3,1,l}=
 \left\{\begin{array}{c}-0.91\\-0.29\\0.418\end{array}\right\} +
 \left\{\begin{array}{c}0.75\\1\\-0.418\end{array}\right\} =
	 \left\{\begin{array}{c}-016\\0.71\\0\end{array}\right\} \]
Once again the free body diagram of beam $1,3$ is used to compute all other
internal and end forces.
\\
\insFigUp{sa-exer2-ex1mod-freebody.pdf}{0.4\textwidth}
\\
\[ M_{1,3}=-M_{3,1}-2.5Y_{3,1}+1\cdot2.5\cdot1= 0-1.77+2.5=-0.72\]
The moment at the center of beam $1,3$ is, when we adopt an ad hoc sign convention
positive when pulling at lower fibers:
\[ M_c= M_{3,1}+Y_{3,1}\cdot 1.25- 1\cdot 1.25\cdot 1=0+0.71\cdot1.25- 1\cdot1.25/2=
 0.26\] 
The end forces of beam $2,3$ are simpler since the beam is not loaded:
\[ \{S\}_{3,2}= [K]_{3,3,l}^{2,3}[T]_{2,3}\{u_3\}=\{-0.726,0.045,0\} \]
The rest is left for students, internal forces diagrams are given for check:
\\
\insFigUp{sa-exer2-ex1mod-diags.pdf}{\textwidth}
\\
The rational convention for drawing the internal forces diagram is this:
\emph{ Moments are drawn on the tension side of the base line without signs,
shear force and normal force are drawn to intuition or feeling,
mostly positive values upwards, BUT their values must have correct signs}.
\\
HW2:\\
Solve for and draw the internal forces diagrams of the frame. In all beams
$EJ=120$, $EA=10$. The displacement vector of joint $3$ is given for check.
If this were a test task, the vector would also be given since there are more
than 2 DOFs.
\\ \[ \{u\}_3^T=\{15.2,0,-3.225\}\]
\\
\insFigUp{sa-exer2-hw.pdf}{0.6\textwidth}
\end{document}

\newpage
HW2 solution
\\ \\
\[ k_{2,3}=k_{3,4}=33.9,~~~k_{1,3}=48,~~~a_{2,3}=a_{3,4}=1.41,~~~a_{1,3}=2\]
\[[K]_{3,3,l}^{2,3}=
\left[\begin{array}{ccc}1.41&0&0\\&4.08&-14.4\\&&67.9\end{array}\right],~~~
[T]_{2,3}=\left[\begin{array}{ccc}0.707&0.707&0\\-0.707&0.707&0\\0&0&1\end{array}\right]
	\]
\[[K]_{3,3,l}^{3,4}=
\left[\begin{array}{ccc}1.41&0&0\\&4.08&14.4\\&&67.9\end{array}\right],~~~
[T]_{3,4}=\left[\begin{array}{ccc}0.707&-0.707&0\\0.707&0.707&0\\0&0&1\end{array}\right]
	\]
\[[K]_{3,3,l}^{1,3}=
\left[\begin{array}{ccc}2&0&0\\&-11.5&-28.8\\&&96\end{array}\right],~~~
[T]_{1,3}=\left[\begin{array}{ccc}0&1&0\\-1&0&0\\0&0&1\end{array}\right]
	\]
\[[K]_{3,3,g}^{2,3}=
\left[\begin{array}{ccc}2.74&-1.33&10.2\\&2.74&-10.2\\&&67.9\end{array}\right],~~~
[K]_{3,3,g}^{3,4}=
\left[\begin{array}{ccc}2.74&1.33&10.2\\&2.74&10.2\\&&67.9\end{array}\right],~~~
[K]_{3,3,g}^{1,3}=
\left[\begin{array}{ccc}11.5&0&28.8\\&2&0\\&&96\end{array}\right],~~~
	\]
\[ [K]= \left[\begin{array}{ccc}17&0&49.2\\&7.49&0\\&&231\end{array}\right],~~~
	\{F\}=\left\{\begin{array}{c}-100\\0\\0\end{array}\right\},~~~
		\{u\}=\left\{\begin{array}{c}15.2\\0\\-3.22\end{array}\right\}
		\]
\[ \{u\}_{3,2,l}=\left\{\begin{array}{c}10.7\\-10.7\\-3.22\end{array}\right\},~~~
	\{S\}_{3,2,l}=\left\{\begin{array}{c}15.2\\2.65\\-64.1\end{array}\right\}\]
\[ \{u\}_{3,4,l}=\left\{\begin{array}{c}10.7\\10.7\\-3.22\end{array}\right\},~~~
	\{S\}_{3,4,l}=\left\{\begin{array}{c}15.2\\-2.65\\-64.1\end{array}\right\}\]
\[ \{u\}_{3,1,l}=\left\{\begin{array}{c}0\\-15.2\\-3.22\end{array}\right\},~~~
	\{S\}_{3,1,l}=\left\{\begin{array}{c}0\\-82.2\\128\end{array}\right\}\]
\\
\insFigUp{sa-exer2-hw-sol.pdf}{\textwidth}

\end{document}
